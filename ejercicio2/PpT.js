function pcChose (){
    const randomN = Math.floor(Math.random() * 3)

    if (randomN === 0) {
        console.log(' El pc eligio...piedra')
        return 'piedra'
    }
    if (randomN === 1) {
        console.log('El pc eligio... papel')
        return 'papel'
    }
    if (randomN === 2) {
        console.log(' El pc eligio... tijera')
        return 'tijera'
    }
}


function choseUser () {
    let user
    do {
        user = prompt('Piedra, Papel, Tijera?').toLowerCase() 
    }while(user !== 'piedra' && user !== 'papel' && user !== 'tijera')

    return user

}

function resultado () {

    let userP = 0
    let pcP = 0
    
    do {
        let user = choseUser()
        let pc = pcChose ()

        if (user === pc){
            console.log('Empate')
        }else if ((user === 'piedra') && (pc === 'tijera')) {
            console.log('punto para user')
            userP++
        }else if ((user === 'papel') && (pc === 'piedra')) {
            console.log('punto para user')
            userP++
        }else if ((user === 'tijera') && (pc === 'papel')) {
            console.log('punto para user')
            userP++
        }else {
            console.log('Punto para Pc')
            pcP++
        }
        console.log('Puntos User',userP)
        console.log('Puntos Pc',pcP)
    } while(userP < 3 && pcP < 3);

    if (userP === 3){
        return 'Gano el usuario'
    }else{
        return 'Gano el pc'
    }

    
}

resultado()




    