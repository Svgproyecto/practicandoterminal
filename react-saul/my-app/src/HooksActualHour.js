
const { useEffect } = require("react");
const { useState } = require("react/cjs/react.development");
/* 

CUSTOM HOOK PARA REFRESCAR LA HORA CADA SEGUNDO

*/
//VAMOS ha hacer un CUSTOM HOOK para refrescar la hora caada segundo, a diferencia de en el ejemplo anteriro 
//en el componente Header llamaremos a nuestra ejecucuion de la funcion para que nos devuleva el dato que queremso, en este caso la hora actual y se refresque el componente cada segundo
function useActualHour() {
    const [actualDate, setActualDate] = useState(new Date())
    useEffect(() => {
        const timer = setInterval(
            () => setActualDate(new Date()),
            1000
        )
        return(()=> clearInterval(timer))
    },[])
    return actualDate
}


const HooksActualHour = () => {
   


    //Array de objetos mensaje que debemos recorrer e imprimir
    const messages = [
        {
          id: 1,
          author: 456317,
          body: "mensaje 0 con un texto algo largo. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
          date: "2019-03-26T18:33:00"
        },
        {
          id: 2,
          author: 456317,
          body: "mensaje 1 de texto suficientemente largo Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
          date: "2019-03-26T18:33:02"
        },
        {
          author: 456326,
          body: "Yo... no soy tu padre",
          date: "2019-03-27T09:33:41.579Z",
          id: 3
        }
      ]
    //Devolvemos un elemento pintable como en todo componente (le pasamos otros componentes que pintan una parte especifica de la pagina, y en el caso de menssages como props le pasamos el map que recorre el array de oobjetos mensaje,(va entre llaves porque el map es un elemento js no jsx))
    return (
        <main className="bodyMessages">
            <section className="chat">
                <Header/>
                {
                    messages.map(message => <Messages key={message.id} message={message}/>)
                }
            </section>
        </main>
    )
}

//Componente que pinta messages, como props le pasamos cada uno de lso mensajes del map anteriro 
const Messages = ({message}) => {
    //destructurion de el objeto mensaje
    const {author, body, date} = message
    const hour = new Date(date)
    return(
        //pintamos el componente mensaje y le pasamos como avatar otro componente independiente 'Avatar'
        <section className="message">
                    <Avatar name={author}/>
                    <section className="text">
                        <header>Enviado a las {hour.toLocaleTimeString('es-ES')}</header>
                        <p>
                            {body}
                        </p>
                    </section>
                </section>
    )
}

const Header = () => {
    const date = useActualHour()
    return(
        <header className="chatHeader">{date.toLocaleTimeString()}</header>
    )
}

//al llamar el componente avatar le estamos pasando el props de author que aqui para practicar hemos decidido llamar name
const Avatar = ({name}) => {
    return(
        <figure className="avatar">
            <img src="/avatars/456318.png" alt="img avatar"/>
            <figcaption className="avatarName">{name}</figcaption>
        </figure>
    )
}

export default HooksActualHour