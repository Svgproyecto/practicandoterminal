import './FormularioEjercicio.css'

import React, {useState} from "react";



const FormularioEjercicio = () => {
    const messagesList = [
        {
          id: 1,
          author: 456317,
          body: "mensaje 0 con un texto algo largo. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
          date: "2019-03-26T18:33:00"
        },
        {
          id: 2,
          author: 456317,
          body: "mensaje 1 de texto suficientemente largo Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
          date: "2019-03-26T18:33:02"
        },
        {
          author: 456326,
          body: "Yo... no soy tu padre",
          date: "2019-03-27T09:33:41.579Z",
          id: 3
        }
      ]

      const [messages, setMessages] = useState(messagesList)//le pasamos el valor inicial

      return (
        <main className="bodyMessages">
            <section className="chat">
                <Header/>
                {
                    messages.map(message => <Messages key={message.id} message={message}/>)
                }
            </section>
            <Formulario messages={messages} setMessages={setMessages}/>
        </main>
    )
}

//Componente que pinta messages, como props le pasamos cada uno de lso mensajes del map anteriro 
const Messages = ({message}) => {
    //destructurion de el objeto mensaje
    const {author, body, date} = message
    const hour = new Date(date)
    return(
        //pintamos el componente mensaje y le pasamos como avatar otro componente independiente 'Avatar'
        <section className="message">
                    <Avatar name={author}/>
                    <section className="text">
                        <header>Enviado a las {hour.toLocaleTimeString('es-ES')}</header>
                        <p>
                            {body}
                        </p>
                    </section>
                </section>
    )
}

 const Header = () => {
    const date = new Date()
    const format = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    return(
        <header className="chatHeader">{format}</header>
    )
}

//al llamar el componente avatar le estamos pasando el props de author que aqui para practicar hemos decidido llamar name
const Avatar = ({name}) => {
    return(
        <figure className="avatar">
            <img src={`/avatars/${name}.png`} alt="img avatar"/>
            <figcaption className="avatarName">{name}</figcaption>
        </figure>
    )
}




const Formulario = ({messages, setMessages}) => {

    


    const handleClick = (e) => {
        e.preventDefault()
        console.log(e);
        const userMessage = e.target.elements.message.value;
        e.target.elements.message.value=""
        const date = new Date().toISOString()
        console.log(date);
        console.log("AQUI ESTOY", userMessage) 
        const newMessage = {
            id: 5,
            author: 456326,
            body: userMessage,
            date: date
        }
        console.log(newMessage);
        setMessages([...messages, newMessage]);
    }

    
    //VOY A PONER EL INPUT DENTRO DEL LABEL AL QUE CORRESPNDE, POR LO QUE NO HARIA FALTA PONERLE Un for = ID  del INPUT, NOBSTANTE LO VOY HA HACER POR REPASAR. (ADEMAS EN JSX EL FOR SE ESCRIBE htmlFor)
    return (
        <main className='formulario'>
            <form name='formMessage' onSubmit={handleClick}>
                <label htmlFor='message'>
                    Nombre:   
                    <input type='text' id='message' placeholder='Escribe aqui tu mensaje' autoComplete='off'/>
                </label>
                <button type='submit'>Enviar</button>
            </form>
        </main>
    )

}






export default FormularioEjercicio;