import './ClickApp.css'
//importamos useStete de react
//esto es un hook de la libreria react
import React, { useState } from "react";


import {CounterContext} from './index'


const ContextClickCount = () => {
    const value = React.useContext(CounterContext)

    console.log('Valor de value, que viene del contexto global', value);
   
    return (
        //El "usuario" eligue el valor de iniciacion del contador
        <ClickCount initValue={value}/>     
    )
}


const ClickCount = ({initValue}) => {
    //Puede que sean propiedades que le hacen falta a useState como si fusese un destructurin del mismo
    //contador es una variable del estada(useState), el numero inicial del contador en este caso 
    //setContador una funcion que actualia con el evento click en este caso 
   const [contador, setContador] = useState(initValue ?? 0); //le pasamos el valor inicial
   //Funcion manejadora del click, actualiza el estado del contador (setContador)
   const updateCounterPlus = (event) => { 
       console.log(event); //informacion sobre el evente con sus datos correspondientes
       setContador(contador + 1)
   }
    //Funcion manejadora del click, actualiza el estado del contador (setContador)
   const updateCounterLess = (e) => {
       console.log(e); //informacion sobre el evente con sus datos correspondientes
       setContador(contador - 1)
   }




  


    return(
        <section className='counter'>
            <h1>Numero de clicks: {contador}</h1>
            <button onClick={updateCounterPlus}>
                Click para sumar
            </button>
            <button onClick={updateCounterLess}>
                Click para restar
            </button>
        </section>
    )

 
}

export default ContextClickCount

