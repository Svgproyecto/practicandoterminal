import { Link, NavLink } from "react-router-dom"


const Nav = () => {
    return(
        <nav>
            <Link to="/">Inicio</Link> |{" "}
            <NavLink to="register" style={({isActive}) => 
                isActive ? {color:'green'} : undefined}> Register |{" "}
            </NavLink>
            <Link to="login">Login |{" "}</Link>
            <Link to="menssages">Messages</Link>
        </nav>
    )
}
export default Nav