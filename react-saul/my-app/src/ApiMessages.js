import React, { useState } from "react";

const ApiMessages = () => {

    const [messages, setApi] = useState([])
    const loadMessages = async (e) => {
        try {
            const res = await (await fetch('http://localhost:3050/messages')).json()
            setApi(res)
        }catch (error){
            console.error(error);
        }
    } 
    return (
        <>
            <button onClick={loadMessages}>Load</button>
            {           
                messages.map(message => <Messages key={message.id} body={message.body} author={message.author} date={message.date}/>)
            }
          
        </>
    )
}

const Messages = ({body, author, date}) => {
    
    return(
        <>
            <Header date ={date}/>
            <main>
                <Avatar name={author}/>
                    <p>{body}</p>
                
            </main>
       </>
    )
}

const Avatar = ({name}) => {
    return(
        <figure className="avatar">
            <img src="/avatars/456318.png" alt="img avatar"/>
            <figcaption className="avatarName">{name}</figcaption>
        </figure>
    )
}
const Header = ({date}) => {
    return(
        <header className="chatHeader">{date}</header>
    )
}

export default ApiMessages