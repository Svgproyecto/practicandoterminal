import {useContext, useState} from "react";
import {TokenContext} from './index'



function Register() {
    const [userEmail, setUserEmail] = useState('')
    const [userPassword, setuserPassword] = useState('')
    const [token, setToken] = useContext(TokenContext) 

    const userRegister = async (e) => {
        e.preventDefault()

        try{
            const dataLogin = {
                "email": userEmail,
                "password": userPassword,
            }

            const res = await fetch("http://localhost:3050/register", {
                    method:'POST', 
                    body: JSON.stringify(dataLogin),
                    headers:{
                        "Content-type":"application/json"
                    } 
                })
        
                const body = await res.json()
                console.log("token", body.accessToken); 
                //esto me escribiria el tipo de error si lo tuviese
                console.log('BODY-->',body);

                
                if(res.ok) {
                    setToken(body.accessToken)
                }else{
                    console.error('Error en la llamada a la API');
                }
               
        }catch (error) {
            console.error(error);
        }
    }

    return(
        <div className="Register">
          {/* {token} */}
            <h1>Register</h1>
            <form onSubmit={userRegister}>
                <RegisterEmail userEmail={userEmail} setUserEmail={setUserEmail}/>
                <RegisterPassword userPassword={userPassword} setuserPassword={setuserPassword}/>
                <button type="submit">Register</button>
            </form>
        </div>

    ) 
}


export default Register;




const  RegisterEmail = ({userEmail, setUserEmail}) => {
    const valueUserEmail = (e) => {
        console.log('cambioEmail');
        setUserEmail(e.target.value)
    }

    return(
      <div className="loginEmail">      
        <label>
          Email:
          <input 
            id = 'emailUser'
            type={'text'} 
            value={userEmail}
            onChange={valueUserEmail}
            placeholder="Escibe tu email aqui"
            />  
        </label>
      
      </div>
  
    ) 
  }


const  RegisterPassword = ({userPassword, setuserPassword}) => {
    const [show, setShow] = useState(false)
    const handleClick = () => show ? setShow(false) : setShow(true);


    const valueUserPassword = (e) => {
        console.log('cambiopass');
        setuserPassword(e.target.value)
    }
  
    
    return(
      <div className="loginPassword">
      
        <label>
          Password:
          <input
            id='passwordUser' 
            type={show ? 'text' : 'password'} 
            value={userPassword}
            onChange={valueUserPassword}
            placeholder="Escibe tu password aqui"
            />
            <button type={'button'} onClick={handleClick}>{show ? 'Ocultar' : 'Mosrtar'}</button>
  
        </label>
      
      </div>
  
    ) 
  }
