import React,{useState} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';


//Desde aqui es lo que yo decido importar (el resto venia por defecto)
import App from './App';
import {BorderRed} from './App'
import Styles from './Styles';
import Chat1 from './Chat1'
import MyChat from './ChatGood';
import ClickApp from './ClickCount'
import FormularioEjercicio from './FormularioEjercicio'
import ListKeyKo from './ListKeyKo'
import FormularioEjercicio2 from './FormulariEjercicio2'
import AlumnosClase from './AlumnosClase'
import ApiMessages from './ApiMessages'
import FormularioControlado from './FormularioControlado'
import ShowWithClick from './tutoStefano/ShowWithClick'
import ContadorStefano from './tutoStefano/ContadorStefano'
import Hookuseefect from './Hookuseefect'
import HooksCargarMensajes from './HooksCargarMensajes'
import HooksActualHour from './HooksActualHour'
import HookListaUsuarios from './HookListaUsuarios'
import ContextClickCount from './ContextClickCount'
import Login from './Login'
import { useLocalStorage } from './useLocalStorage';
import Register from './Register';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';






const name = 'Pepe'
const apellido = 'Rodrigez' 


/**
 * ##########################
 * ## Ejemplos que hacemos ##
 * ##########################
 */


/* dentro de <React.StrictMode> 

  ####################
  ## Ejemplo de App ##
  ####################
  -->Cremos un componente borde rojo y lo ponemos en toda la pagina, ademas de llamar aqui en el archivo principal (aqui) dos de las props que se utilizan

      <BorderRed width={20}>    
        <App name={name} surname="Rodolfo" answere={answere}>esto es un children (prop que hemos importado de app)</App>
      </BorderRed>
   

  ######################
  ## Ejemplo de Style ##
  ######################
  -->
       <Styles/>

  ######################
  ## Ejemplo de Chat1 ##
  ######################
  -->
       <Chat1/>
    
  #######################
  ## Ejemplo de MyChat ##
  #######################
  -->

       <MyChat/>

  #########################
  ## Ejemplo de ClickApp ##
  #########################
  -->

       <ClickApp/>

  ###########################
  ## Ejemplo de Formulario ##
  ###########################
  -->

       <FormularioEjercicio/>

  ###########################
  ## Ejemplo de Formulario ##
  ###########################
  -->

       <ListKeyKo/>

  ###########################
  ## Ejemplo de Formulario 2 ##
  ###########################
  -->

       <FormularioEjercicio2/>

  ###########################
  ## Ejemplo de AlumnosClase  ##
  ###########################
  -->

       <AlumnosClase/>

  ###########################
  ## Ejemplo de ApiMessages  ##
  ###########################
  -->

       <ApiMessages/>

  ###########################
  ## Ejemplo de FormularioControlado  ##
  ###########################
  -->

       <FormularioControlado/>

  ###########################
  ## Ejemplo de hookuseefect  ##
  ###########################
  -->

       <Hookuseefect/>

  ###########################
  ## Ejemplo de HooksCargarMensajes  ##
  ###########################
  -->

       <HooksCargarMensajes/>

  ###########################
  ## Ejemplo de HooksActualHour  ##
  ###########################
  -->

       <HooksActualHour/>



  ###########################
  ## Ejemplo de HookListaUsuarios  ##
  ###########################
  -->

       <HookListaUsuarios/>


###########################
## Ejemplo de contexto  ##
###########################
  -->

     <MyContext.Provident value='dark'>
          <ContextClickCount/>
     </MyContext.Provident>





     <TokenProvider>
     <Login/>
    </TokenProvider>

    <TokenProvider>
     <Register/>
    </TokenProvider>


###########################
## Ejemplo de Login  ##
###########################
-->

     <Login/>

       



//TUTOSTEFANO
  ###########################
  ## Ejemplo de ShowWithClick  ##
  ###########################
  -->

     <ShowWithClick/>
     

 */

//Definimos de manera global lo que va a valer al CounerContext en este caso tiene un value={[count, setCount]}, es decir tiene el valor inicial que le demos a useState y ademas la posibilidad de cambairlo hay donde lo importe
//y envolvemso toda la 'pagina' con el valor de 'ConterProvident'
//este ejemplo es pobre   

export const CounterContext = React.createContext()


const CounterProvident = (props) => {
     /* const [count, setCount] = useState(10) */
    return( 
          <CounterContext.Provider value={10/* [count, setCount] */}>
              {props.children}
        </CounterContext.Provider>
   )
}



export const TokenContext = React.createContext()

const TokenProvider = (props) =>{
     const [token, setToken] = useLocalStorage('token')

     return(
          <TokenContext.Provider value={[token, setToken]}>
               {props.children}
          </TokenContext.Provider>
     )
}

ReactDOM.render(
  <React.StrictMode>
    
    <TokenProvider>
         <BrowserRouter>
               <Nav/>
               <Routes>
                    <Route path='/' element={<ListKeyKo/>}/>
                    <Route path='register' element={<Register/>}/>
                    <Route path='login' element={<Login/>}/>
                    <Route path='menssages' element={<HooksCargarMensajes/>}/>    
               </Routes>
         </BrowserRouter>
    </TokenProvider>
     
  </React.StrictMode>,
  document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
