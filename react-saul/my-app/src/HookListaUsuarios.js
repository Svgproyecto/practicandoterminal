import './ApiMessages.css'
const { useEffect } = require("react");
const { useState } = require("react/cjs/react.development");

/* 

Vamos a hacer un select de los usuarios y al seleccionar uno haremso que se muestre su avatar

*/



//Creamos el custom HOOK
const useUpdateUsers = () => {

    const [users, setUsers] = useState([])

    const loadUsers = async (e) => {
        //setError('')
        try {
            const res = await (await fetch('http://localhost:3050/people')).json()
            setUsers(res)
        } catch (error) {
            //setError('Ha ocurrido un error')
            console.error('Error-->',error);
        }
    }

    useEffect(() => {
            loadUsers()
    },[])


    return users
}


/* ................................................................. */



function HookListaUsuarios() {
    
    
    
    const usersServer = useUpdateUsers()
    const [selectedUser, setSelectedUser] = useState(null)
    //aqui deveriamos combinar los dos ararys como aprendimos en la prueba de control del los animales y las pruebas
    //MAL -> const nameUser = usersServer.map(name => name.name)
    

    
    return (
        <>
        <h1>Lista de personajes</h1>
        <p>Personaje seleccionado: {selectedUser}</p>
            {usersServer ? 
                <select onChange={(e)=> setSelectedUser(e.target.value)}>
                    <option value=''>Selecciona personaje</option>
                    {usersServer.map(user => <option key={user.id} value={user.id}>{user.name}</option>)}
                </select> 
            : null
            }
            {selectedUser? <div className='ficha'><Avatar id={selectedUser} name={''}/></div> 
            : null
            }
            
          
        </>
    )
}


const Avatar = ({id, name}) => {
    return(
        <figure className="avatar">
            <img src={`/avatars/${id}.png`} alt="img avatar"/>
            <figcaption className="avatarName">{name}</figcaption>
        </figure>
    )
}

export default HookListaUsuarios