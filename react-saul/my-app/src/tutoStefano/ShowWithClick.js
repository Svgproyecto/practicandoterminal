import './ShowWithClick.css'
import { useState } from 'react';
import Lable from './compnents/lable'


const ShowWithCLick = () => {
    const [hide_showTexto, sethide_showTexto] = useState(false);
    const playText = ()=> {
        sethide_showTexto(!hide_showTexto)
    }

    return(
        <>
            <h1>HOlA desde JSX vamos ha hacer desaparecer un componente</h1>
            <div className='seccionDesaparece'>
                
                <button className='hide_show' onClick={playText}>Click me</button>
                {hide_showTexto && <Lable/>}
            </div>
        </>

    )
   
}
export default ShowWithCLick;