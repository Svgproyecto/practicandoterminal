import { useState } from "react";
import Button from './compnents/button'
import './ContadorStefano.css'

const ContadorStefano = () => {
    const [contador, setContador] = useState(0)
    const suma = () => {
        setContador(contador + 1)
    }
    const resta = () => {
        setContador(contador - 1)
    }
    const start = () => {
        setContador(0)
    }
    const style = {color : contador < 0 ? 'red' : 'blue'}

    const colorClass = contador < 0 ? "white" : 'yellow'
    
    
    return (
        <>
            <section>
                <h1 style={style}>Contador sin className</h1>
                <h2 className={colorClass}>Contador con className</h2>
                <span>{contador}</span>
                <Button onClick={suma}>Suma 1</Button>
                <Button onClick={start}>resetea</Button>
                <Button onClick={resta}>Resta 1</Button>


            </section>
        </>
    )
}

export default ContadorStefano