
import { Navigate } from 'react-router-dom';
import { TokenContext } from '.';
import './ApiMessages.css'
const { useEffect, useContext } = require("react");
const { useState } = require("react/cjs/react.development");


/* 

CUSTOM HOOK PARA CARGAR MENSAJES AUTOMATICAMENTE DE LA API  

*/
//VAMOS ha hacer un CUSTOM HOOK para refrescar la hora caada segundo, a diferencia de en el ejemplo anteriro 
//en el componente Header llamaremos a nuestra ejecucuion de la funcion para que nos devuleva el dato que queremso, en este caso la hora actual y se refresque el componente cada segundo
//En este ejemplo no la vamos a ejecutar porque falta codigo por escribir


//Creamos el custom HOOK
const useUpdateMessages = () => {
    
    const [messages, setMessages] = useState([])
    const [token, setToken] = useContext(TokenContext) 

    

    const loadMessages = async (e) => {
        //setError('')
        try {
            const res = await (await fetch('http://localhost:3050/660/messages', {
                headers:{
                    "Authorization" : `Bearer ${token}`
                }
            })).json()
            console.log(res);
            setMessages(res)
            
        } catch (error) {
            //setError('Ha ocurrido un error')
            console.error('Error-->',error);
        }
    }

    useEffect(() => {
        const timer = setInterval(
            () => loadMessages(),
            1000
        )
        return(()=> clearInterval(timer))
    },[])

    return messages
}


/* ................................................................. */



function HooksCargarMensajes() {
    
    const [token, setToken] = useContext(TokenContext) 

    
    const messagesServer = useUpdateMessages()
    
    return (
        <>
            {token && (
            <Navigate to='' />
          )}
            {           
                messagesServer.map(message => <Messages key={message.id} body={message.body} author={message.author} date={message.date}/>)
            }
          
        </>
    )
}

const Messages = ({body, author, date}) => {
    
    return(
        <>
            <Header date ={date}/>
            <main>
                <Avatar name={author}/>
                    <p>{body}</p>
                
            </main>
       </>
    )
}

const Avatar = ({name}) => {
    return(
        <figure className="avatar">
            <img src="/avatars/456318.png" alt="img avatar"/>
            <figcaption className="avatarName">{name}</figcaption>
        </figure>
    )
}
const Header = ({date}) => {
    return(
        <header className="chatHeader">{date}</header>
    )
}



   




export default HooksCargarMensajes