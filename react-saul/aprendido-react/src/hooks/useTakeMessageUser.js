import { useParams } from "react-router-dom";
import { TokenContext } from "..";
const { useEffect, useContext } = require("react");
const { useState } = require("react/cjs/react.development");





//CUSTOM HOOK PARA CARGAR UN MENSAJE EN CONCRETO DE UN USUARIO DE LA API  
//los customHook siempre se crean empezado por use...

//VAMOS ha hacer un CUSTOM HOOK para refrescar los mensajes caada segundo.
const useTakeMessageUser = () => {
    const [messages, setMessages] = useState([]) 

    //este useState sirve par controlar si hubiese un proble al cargar la api
    const [error, setError] = useState('')

    const [token, setToken] = useContext(TokenContext) 

      /*  //el Hook 'useParams' nos lo proporciona reactRouter y nos sirve para obtener el parametro que pasamos en la ruta despues de ':'
    //params es un objeto que va a tener dentro los parametros que le pasemos a la ruta, si haces un console.log lo entenderas
    const params = useParams() */
    const params = useParams()

    /* ......................... */
       
    /* ......................... */


    const loadMessages = async (e) => {
        //inicializamod el valor de error a comillas vacias 'falshy'
        setError('')
        try {
            const apiRes = await fetch(`http://localhost:3050/660/messages/${params.idUser}`, {
                headers:{
                    "Authorization" : `Bearer ${token}`
                }
            })
            if(apiRes.status === 404) {setError('Ha habido un error')}  
            
            const body = await apiRes.json()
            setMessages(body)
        } catch (error) {
            setToken(null)
            console.error('Error-->', error);
            //si entrasemaos por el catch existiria un problema y al escribir algo el valor de error pasaria a ser 'truehy'
            setError('Ha ocurrido un problema')
        }
    }

    //creo que este useEffect basta con que llame a la fiuncion una vez, no hace falta el set interval cada segundo para recargal lso mensajes cada segunmdo, en esta app no tendria sentido, en una como whatsap tal vez si, lo dejo comentado abajo porsiacaso, esto lo hice asi simolemente para que no me reventase el pc jajaj
    useEffect(() => {
            loadMessages()
    }, 
    [])

   /*  useEffect(() => {
        const timer = setInterval( 
            loadMessages, 1000
        )
        return(
            ()=> clearInterval(timer)
        )
    }, 
    []) */
    
    //si error es truehy, enviamos a donde invoquemos este hook un valor 'falshy', si error es 'falshy' enviamos el valor de messages 
    return (
        error ? '' : [messages, setMessages]
    )
}


export default useTakeMessageUser