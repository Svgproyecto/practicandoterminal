
const { useEffect } = require("react");
const { useState } = require("react/cjs/react.development");

//Creamos el custom HOOK
const useUserList = () => {

    const [users, setUsers] = useState([])

    const loadUsers = async (e) => {
        //setError('')
        try {
            const res = await (await fetch('http://localhost:3050/people')).json()
            setUsers(res)
        } catch (error) {
            //setError('Ha ocurrido un error')
            console.error('Error-->',error);
        }
    }

    useEffect(() => {
        loadUsers()
    },[])


    return users
}



export default useUserList