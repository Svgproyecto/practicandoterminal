import { TokenContext } from "..";
const { useEffect, useContext } = require("react");
const { useState } = require("react/cjs/react.development");





//CUSTOM HOOK PARA CARGAR MENSAJES AUTOMATICAMENTE DE LA API  
//los customHook siempre se crean empezado por use...

//VAMOS ha hacer un CUSTOM HOOK para refrescar los mensajes caada segundo.
//useUpdateMessages, llamara a la api cada segundo actulizando el valor de messages y exportandolo al fichero 'Messages' para que repinte los mensajes 
const useUpdateMessages = () => {
    const [messages, setMessages] = useState([]) 

    //este useState sirve par controlar si hubiese un proble al cargar la api
    const [error, setError] = useState('')

    const [token, setToken] = useContext(TokenContext) 


    const loadMessages = async (e) => {
        //inicializamod el valor de error a comillas vacias 'falshy'
        setError('')
        try {
            const apiRes = await fetch(`http://localhost:3050/660/messages`, {
                headers:{
                    "Authorization" : `Bearer ${token}`
                }
            })
            if(!apiRes.ok) {setToken(null)} 
            const body = await apiRes.json()
            setMessages(body)
        } catch (error) {
            setToken(null)
            console.error('Error-->', error);
            //si entrasemaos por el catch existiria un problema y al escribir algo el valor de error pasaria a ser 'truehy'
            setError('Ha ocurrido un problema')
        }
    }

    useEffect(() => {
        const timer = setInterval( 
            loadMessages, 1000
        )
        return(
            ()=> clearInterval(timer)
        )
    }, 
    [] )
    
    //si error es truehy, enviamos a donde invoquemos este hook un valor 'falshy', si error es 'falshy' enviamos el valor de messages 
    return (
        error ? '' : [messages, setMessages]
    )
}


export default useUpdateMessages