import { useEffect, useState } from "react"

const useGetFiles = () => {



const [getFiles, setGetFiles] = useState([])

    const loadFile = async (e) => {

        try {
            const allFiles = await (await fetch('http://localhost:3050/files', {
                //a una peticion de tipo get no necesitamos pasarle metodo, ya que por defecto es GET, en este caso lo voy a dejar solo por que quede claro
                method: 'GET',
            })).json()
            setGetFiles(allFiles)

        } catch (error) {
            console.error(error);
        }
        
    }

    useEffect(() => {
        const timer = setInterval( 
            loadFile, 1000
        )
        return(
            ()=> clearInterval(timer)
        )
    }, 
    [] )

    return(
        [getFiles, setGetFiles]
    )
}
export default useGetFiles