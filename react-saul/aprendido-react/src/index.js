import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Chat from './Chat'
import Register from './components/Register/Register'

import { useLocalStorage } from './hooks/useLocalStorage';
import { BrowserRouter, Link, Routes, Route, NavLink } from 'react-router-dom';
import Login from './components/Login';
import MessageIdUser from './components/Message/MessageIdUser/MessageIdUser'
import UpFiles from './components/Files/UpFiles/UpFiles';
import GetFiles from './components/Files/GetFiles/GetFiles';
import PreMessage from './components/Message/PreMessage/PreMessaje';







//ESTOS COMPONENTES LOS CREO AQUI PORQUE SOLO SE VAN A USAR A ESTE NIVEL

//Componente para envolver a toda la aplicacion con un contexto para que este dispnible en toda la aplicacion de manera implicita el valro de token
export const TokenContext = React.createContext()
const TokenProvider = (props) => {
  //importamos e utilizamos el customHook de 'useLocalStorage' pasandole como parametro el nombre que queramos que tenga esto qua vamos a guardar en el local storage 
  const [token, setToken] = useLocalStorage('token')
  return(
    <TokenContext.Provider value={[token, setToken]}>
      {props.children}
    </TokenContext.Provider>
  )
}


//Componente que va a envolver a la aplicacion, en este caso de a modo de barra de navegacion
//Los Link son comonentes imprtados de la libreria ReactRouter

const MainMenu = () => {
  return(
    <nav>
      <Link to='/'>Inicio</Link> |{' '}
      <Link to='register'>Register</Link> |{' '}
      <Link to='login'>Login</Link> |{' '}
      {/* Ejemlo de como usar 'NavLink', tambien componente de la libreria react router, su esqueleto es asi y podemos cambiar el comportamiento depende este activado o no el link  */}
      <NavLink 
        to='upFiles'
        style={ ({isActive}) => isActive ? {color:'green'} : undefined}>
          Subir archivos
      </NavLink>|{' '}
      <Link to='getFiles'>Ver archivos</Link>|{' '}
      {/* Link un poco initil ya que de momento debemos escribir el param 'idUser a mano */}
      <Link to='messages/:idUser'>Ver mensajes de...</Link>

    </nav>
  )
}





//importo el Componente chat y lo llamo apra pintar dentro de el elemento con el id 'root'

//Envolvemos nuestra aplicacion con un contexto sobre el token para que podamso acceder a el en cualquier sitio de nuestra app
//Envolvemos a nuestra app con unos componentes de la libreria ReactRouter (BrowserRouter, Routes, Route) y ademas invocamos a nuesto componente que pinta el nav (MainMenu), ni que decir que hace falta instalar esta libreria(en la documentacion esta como) e importar los componentes que nos hagan falta, en este caso los anterirmente nombrados
ReactDOM.render(
  <React.StrictMode>
    <TokenProvider>
      <BrowserRouter>
        <MainMenu/>
        <Routes>
            
            <Route path='/' element={<Chat/>}/>
            <Route path='register' element={<Register/>}/>
            <Route path='login' element={<Login/>}/>
            <Route path='*' element={<div>La pagina no existe</div>}/>
            <Route path='upFiles' element={<UpFiles/>}/>
            <Route path='getFiles' element={<GetFiles/>}/>
            {/* Aqui estamos anidando ':idUser' dentro de messages, por lo que la ruta del primero seria 'http://localhost:3000/messages' y la del componente anidado 'http://localhost:3000/messages/:idUser' */}
            <Route path='messages' element={<PreMessage/>}>
              <Route path=':idUser' element={<MessageIdUser/>}/>
            </Route>
            
        </Routes>
      </BrowserRouter>
    </TokenProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
