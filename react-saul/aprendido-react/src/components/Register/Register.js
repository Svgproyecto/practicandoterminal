import {useContext, useState} from "react";
import { Navigate } from "react-router-dom";
import {TokenContext} from '../../index'
import './register.css'


//Creamos un componente para registrarnos 
function Register() {
    const [userEmail, setUserEmail] = useState('')
    const [userPassword, setUserPassword] = useState('')
    const [token, setToken] = useContext(TokenContext) 

    //creamos la funcion manejadora del boton del formulario
    const userRegister = async (e) => {
        e.preventDefault()

        //intentamos registrar al usuario con una peticion de tipo POST que debe tener este cuerpo 
        try{
            const dataLogin = {
                "email": userEmail,
                "password": userPassword,
            }

            //en esta ruta y al pasarla el body anterior, genera un token con esos datos
            const res = await fetch("http://localhost:3050/register", {
                    method:'POST', 
                    body: JSON.stringify(dataLogin),
                    headers:{
                        "Content-type":"application/json"
                    } 
                })
        
                const body = await res.json()
                console.log("token", body.accessToken); 

                //si todo a ido bien cogemos la propiedad 'accessToken' del body (es decir el token) y actualizamos el valor de token gracias a el contexto de token context(definido en index.js), que envuleve toda la apliccacionlo tendremso disponible para toda la app
                if(res.ok) {
                    setToken(body.accessToken)
                }else{
                    console.error('Error en la llamada a la API');
                }
               
        }catch (error) {
            console.error(error);
        }
    }

    //pinintamos el formulario de registro
    return(
      <>
      {/* Aqui indicamos que si tenemso ya un token guardado en el localStorage, nos rediriga a la pafina de inicio, en este caso la de los mensajes, necesario importar el componente 'Navigate' de la libreria 'ReactRouter' y el valor de token, guardado en el Contexto*/}
      {token && (
                <Navigate to='/'/>
            )}
        <div className="register">
            <p>Register</p>
            <form onSubmit={userRegister}>
                <RegisterEmail userEmail={userEmail} setUserEmail={setUserEmail}/>
                <RegisterPassword userPassword={userPassword} setuserPassword={setUserPassword}/>
                <button type="submit">Register</button>
            </form>
        </div>
      </>

    ) 
}


export default Register;


//componentes de registrer 


const  RegisterEmail = ({userEmail, setUserEmail}) => {
    const valueUserEmail = (e) => {
        console.log('cambioEmail');
        setUserEmail(e.target.value)
    }

    return(
      <div className="registerEmail">      
        <label>
          Email:
          <input 
            id = 'emailUser'
            type={'text'} 
            value={userEmail}
            onChange={valueUserEmail}
            placeholder="Escibe tu email aqui"
            />  
        </label>
      
      </div>
  
    ) 
  }


const  RegisterPassword = ({userPassword, setuserPassword}) => {
    const [show, setShow] = useState(false)
    const handleClick = () => show ? setShow(false) : setShow(true);


    const valueUserPassword = (e) => {
        console.log('cambiopass');
        setuserPassword(e.target.value)
    }
  
    
    return(
      <div className="registerPassword">
      
        <label>
          Password:
          <input
            id='passwordUser' 
            type={show ? 'text' : 'password'} 
            value={userPassword}
            onChange={valueUserPassword}
            placeholder="Escibe tu password aqui"
            />
            <button type={'button'} onClick={handleClick}>{show ? 'Ocultar' : 'Mosrtar'}</button>
  
        </label>
      
      </div>
  
    ) 
  }

 
  