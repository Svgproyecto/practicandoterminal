import useTakeMessageUser from "../../../hooks/useTakeMessageUser"
import Avatar from "../../Avatar/Avatar"




const MessageIdUser = () => {
 

    const [message , ,] = useTakeMessageUser()
    return(
       message ?  
       <div>
        <h1>Mensaje, el param es: {/* {params.idUser} */}</h1>
        <div className='aMessage' key={Math.random()*1000}>
            <header>
               {message.date}
            </header>
            <div className='orderBodyMessage'>
               <Avatar id={message.author} />
               <p className='worlds'>{message.body}</p>
            </div>
        </div>
    </div> 
    : <div>Usuario no encontrado</div>
    )
}
export default MessageIdUser