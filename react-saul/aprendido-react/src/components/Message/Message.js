import './message.css'
import Avatar from '../Avatar/Avatar'

//este use state solo es para poder probar la practica del button
import { useState } from 'react/cjs/react.development'
import { Navigate } from 'react-router-dom'
import { useContext } from 'react'
import { TokenContext } from '../..'



const Message = ({messages, setMessages}) => {
    /* const [messages, setMessages] = useUpdateMessages() */

//AQUI importamos un custom hook que llama a la api y actuliza los mensajes cada segundo 'useUpdateMessages'
//si comentamos esa linea y descomentamos la del el 'button' en el return + lo que hay justo aqui debajo, podremos observar una de las practicas que hicimos durante la clase con un botn para cargar los mensajes de la API

   /*  const [messages, setMessages] = useState([])
    const loadMessages = async () => {
        try {
            const res = await( await fetch('http://localhost:3050/messages')).json()
            setMessages(res)
        } catch (error) {
            console.error(error);
        }
    } */

    //ERROR POR CONSOLA PREGUNTAR POR QUE
    /* const messages = useUpdateMessages() */

    //si messages (valor que devuelve nuestro cusmoHook de useUpdateMessages), es truehy (es decir continen algo), hacemos un map de messages( datos de la api)
    //si el valor de messages es 'falshy' (cdena vacia), salimos por la segunda opcion que pinta un error 

    const [token, setToken] = useContext(TokenContext)

    return (
        messages ? (
            <>
            {/* Aqui indicamos que si no tenemso un token guardado en el localStorage, nos rediriga a la pafina de registro, necesario importar el componente 'Navigate' de la libreria 'ReactRouter' y el valor de token, guardado en el Contexto*/}
            {!token && (
                <Navigate to='register'/>
            )}
            {           
                messages.map(message => {
                    return( 
                        <div className='aMessage' key={Math.random()*1000}>
                            <header>
                                {message.date}
                            </header>
                            <div className='orderBodyMessage'>
                                <Avatar id={message.author} />
                                <p className='worlds'>{message.body}</p>
                            </div>
                        </div>
                    )
                    
                }
                
                )}
            {/* <button onClick={loadMessages}>Load</button> */}
            
        </>
        ) : <div style={{color: 'red'}}>Ha habido un error</div>
    )
}



export default Message;