import './selectUser.css'
import useUserList from "../../hooks/useUserList"
import Avatar from "../Avatar/Avatar"


function SelectUser({selectedUser, setSelectedUser}) {
    
    const userList = useUserList()
    
    //este dato lo hemos subido un nivel en jerarquia (a chat) porque tambien nos hacia falta el dato en Send messages para pintar el autor del mensaje 
    /* const [selectedUser, setSelectedUser] = useState(null) */
    //aqui deberiamos combinar los dos ararys como aprendimos en la prueba de control del los animales y las pruebas
    
    

    
    return (
        
        <div className='selectUser'>
            <div className='selectUserAling'>
            <p>Personaje seleccionado:</p>
            {userList ? 
                <select onChange={(e)=> setSelectedUser(e.target.value)}>
                    <option value=''>Selecciona personaje</option>
                    {userList.map(user => <option key={user.id} value={user.id}>{user.name}</option>)}
                </select> 
            : null
            }
            </div>
            
            {selectedUser? <div className='ficha'><Avatar id={selectedUser} name={''}/></div> 
            : null
            }  
        </div>
        
    )
}


export default SelectUser