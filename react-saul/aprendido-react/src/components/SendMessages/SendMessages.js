import { useState } from "react/cjs/react.development"
import { useLocalStorage } from "../../hooks/useLocalStorage"

 const SendMessages = ({messages, setMessages, selectedUser}) => {
     //gracias a un customHoook guardamos el mensaje de el usuario en el local storage hasta que se envie
     const [userMessage, setUserMessage] = useLocalStorage('message')
     
     //Controlamos un posible estado de error
     const [error, setError] = useState('')
     
     //customHook que actualiza cada segunfo la lista de mensajes de la api 
     //Hemos tenido que subirlo al padre porqeu lo requeriamso en mas ficheros 
     /* const [messages, setMessages] = useUpdateMessages() */
 

     //funcion manejadora del clik 
     const handleClikMessageChange = (e) => {
         setUserMessage(e.target.value)
     }


     const handleClick = async (e) => {
        e.preventDefault()
        setError("")
        //Reseteamos el valor del cuadro de texto a cadena vacia, despues de haber capturado su valor 
        setUserMessage('')
        //Capturamos la hora actual a la que hemos enviado el mensaje
        const date = new Date().toISOString()
        //estos datos vendrian de la base de datos, aqui pongo unos culquiera(la key, que deberia ser el id unico, va a dar error porque no lo es)
        const newMessage = {
            /* id: 5, */
            author: selectedUser,
            body: userMessage,
            date: date
        }
       

        //Actualizamos el array del servidor con nuesto nuesvo array con el mensaje que hemos ahadido
        //utilizamos un try catch para controlar posibles errores
        try {
            //serializamos el nuevo mensaje que vamos a incorporar a la api y relizamos la peticion de tipo POST
            const serialUserMessage = JSON.stringify(newMessage)
            const serverRes = await fetch("http://localhost:3050/messages", {
                method:'POST', 
                body: serialUserMessage,
                headers: {
                    "Content-type":"application/json"
                } 
            })

            //Capturamos el valor de la api y la deserializamos para poder lerla
            const body = await serverRes.json()
            console.log('mensaje enviado al servidor', body);
            
             //actualizamos el arary de mesajes ya existente de la api que hemos obtenido antes, con nuesto nuevo objeto 'newMessage'
            const newMessages = [...messages, body]

            if(serverRes.ok) {
                //llmar a cargar mensajes de la api
                //duda
                setMessages(newMessages)
                console.log('cosas');
            }else {
                setError('Ha habido un ERROR')
            }
        }catch(error) {
            console.error(error);
            setError('Ha habido un error')
        }
     }
     //si el valor de error es trurhy hacemos lanzamos un error, sino nada, pintamos el formulario de escibir un mensaje 
     return ( 
        <>
            {error ? <div style={{color: 'red'}}>{error}</div> : null }
            <form name='formMessage' onSubmit={handleClick} >
                <label htmlFor='message'>
                    Message:   
                    <input type='text' id='message' value={userMessage} onChange={handleClikMessageChange} placeholder='Escribe aqui tu mensaje' autoComplete='off'/>
                </label>
                <button type='submit'>Enviar</button>
            </form>
        </>
     )
 }

 export default SendMessages