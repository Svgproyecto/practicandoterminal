import './avatar.css'

//Componente que pinta la imagen de usuario y su nombre de server (hace falta iniciarlo)
//recibe un prop que es author, que definimos en el fichero 'Message.js' al llamar al componente
const Avatar = ({id, name}) => {

    return(
        <figure className='avatar'>
            <img src={`/avatars/${id}.png`} alt='img avatar' />
            {/* 'No esta del todo bien porque de la base de datos ariamos un join para tener las dos tablas juntas y tener los datos de hambos campos, ahora mismo vamos a poner el id' */}
            <figcaption className='userName'>{id}</figcaption>
        </figure>
    )
}
export default Avatar;