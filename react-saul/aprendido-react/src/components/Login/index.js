import {useContext, useState} from "react";
import { Navigate } from "react-router-dom";
import { TokenContext } from "../..";

import './login.css'



function Login() {
    const [loginUserEmail, setUserEmail] = useState('')
    const [loginUserPassword, setuserPassword] = useState('')
    const [token, setToken] = useContext(TokenContext) 

    const userLogin = async (e) => {
        e.preventDefault()

        try{
            const dataLogin = {
                "email": loginUserEmail,
                "password": loginUserPassword,
            }

            const res = await fetch("http://localhost:3050/login", {
                    method:'POST', 
                    body: JSON.stringify(dataLogin),
                    headers:{
                        "Content-type":"application/json"
                    } 
                })
        
                const body = await res.json()
                console.log("token", body.accessToken); 

                
                if(res.ok) {
                    setToken(body.accessToken)
                }else{
                    console.error('Error en la llamada a la API');
                }
               
        }catch (error) {
            console.error(error);
        }
    }

    return(
        <div className="login">
        {/* Comento esta parte para poder hacer le log out y poner el token a null para poder hacer pruebas, pero tendria mas sentido tenerlo sin desactivar, y que si estas logeado y lo intentas te lleve a otra pagina */}
         {/*  {token && (
            <Navigate to='/'/>
          )} */} 
            <h1>Login</h1>
            <form onSubmit={userLogin}>
                <LoginEmail userEmail={loginUserEmail} setUserEmail={setUserEmail}/>
                <LoginPassword userPassword={loginUserPassword} setuserPassword={setuserPassword}/>
                <button type="submit">Login</button>
            </form>
            <button onClick={()=> setToken(null)}> Log OUT</button>
        </div>

    ) 
}


export default Login;




const  LoginEmail = ({userEmail, setUserEmail}) => {
    const valueUserEmail = (e) => {
        console.log('cambioEmail');
        setUserEmail(e.target.value)
    }

    return(
      <div className="loginEmail">      
        <label>
          Email:
          <input 
            id = 'loginEmailUser'
            type={'text'} 
            value={userEmail}
            onChange={valueUserEmail}
            placeholder="Escibe tu email aqui"
            />  
        </label>
      
      </div>
  
    ) 
  }


const  LoginPassword = ({userPassword, setuserPassword}) => {
    const [show, setShow] = useState(false)
    const handleClick = () => show ? setShow(false) : setShow(true);


    const valueUserPassword = (e) => {
        console.log('cambiopass');
        setuserPassword(e.target.value)
    }
  
    
    return(
      <div className="loginPassword">
      
        <label>
          Password:
          <input
            id='loginPasswordUser' 
            type={show ? 'text' : 'password'} 
            value={userPassword}
            onChange={valueUserPassword}
            placeholder="Escibe tu password aqui"
            />
            <button type={'button'} onClick={handleClick}>{show ? 'Ocultar' : 'Mosrtar'}</button>
  
        </label>
      
      </div>
  
    ) 
  }

 
  