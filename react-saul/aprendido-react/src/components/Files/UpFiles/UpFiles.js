import { useState } from "react"

const UpFiles = () => {

    const [file, setFile] = useState(null)

    const uploadFile = async (e) => {
        e.preventDefault()

        try {
            let data = new FormData()
            data.append('image', file)
            await (await fetch('http://localhost:3050/files', {
                method: 'POST',
                body: data
            })).json()

        } catch (error) {
            console.error(error);
        }
        
    }

    const onFileChange = e => {
        console.log(e.target);
        const f = e.target.files[0]
        setFile(f)
    }

    return(
        <div className="upFiles">
            <form onSubmit={uploadFile}>
                <label>
                    Selecciona el Archivo que deseas subir
                    <input type={'file'} onChange={onFileChange}/>
                </label>
                <button>Subir</button>
            </form>
        </div>
    )
}

export default UpFiles