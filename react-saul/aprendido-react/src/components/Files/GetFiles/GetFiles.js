import useGetFiles from "../../../hooks/useGetFiles";


const GetFiles = () => {

    //custom hook que coge (get), los archivos de la api 
    const [getFiles, ,] = useGetFiles()

    if(getFiles.length === 0) return <h1>No hay archivos</h1>

    return(
        <div className="getFiles">
            <p>AQUI VA LA LISTA DE Fotografias</p>
            {
                getFiles.map(file => {
                    return(
                        <img 
                        key={file.id}
                        src={`http://localhost:3050${file.filename}`}
                        className="file"
                        alt="file"/>
                    )
                })   
            }
        </div>
    )
}

export default GetFiles


