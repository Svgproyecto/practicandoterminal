import './header.css'
//importo de React 'useState' y 'useEffect', son  variables internas de React 
    //useState: almacena el estado de nuestro componente, acepta un valor inicial y devulve un array con dos elementos y el valor de la variable y la funcion para modificar dicho valor
    //useEffect: acepta una funcion como argumento, se ejecuta por defecto cuando se renderiza por primera vez y cada vez que se actualizr el componente que la contiene, se le puede pasar un segundo argumento para especificar cuadno debe ejecutarse, como minimo deberemso pasarle un array vacio
import React, {useState, useEffect} from 'react'


//no necesitamso props (parametros que le pasariamos a el componente Header entre corchetes dentro de los parentesis)
const Header = (/* {props} */) => {

//podriamos hacer un custom HOOK que contuviese los dos useState que veremos mas adelante para hacer la llamada a una api

    //le pasamos a useState el valor inicial de 'new Date()' (fecha y hora actuales)
    const [date, setDate] = useState(new Date())
    
    //El componente se va a repintar cada segundo gracias a el 'setInterval' y ejecutara la funcion 'setDate', con el parametro de 'new Date(), por lo tanto el valor de 'date' sera  la fecha y hora actuales cada segundo
    useEffect(() => {
        let t = setInterval(() => setDate(new Date()), 1000)
        return (() => clearInterval(t))

    },[])

    //formateamos la fecha de la variable date para mostrarlo a nuesto gusto y finalmente escribimos el contenido pintable JSX de el componente Header (que llamaremos en Chat, el componente principal)
    const formatDate = date.toLocaleDateString()+ ' - ' + date.toLocaleTimeString()
    return (
        <header className='headerMessage'>
            {formatDate}
        </header>

    )
}

export default Header