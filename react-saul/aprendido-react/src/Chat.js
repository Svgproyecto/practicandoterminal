//importo los diferentes Componentes que voy a utilizar, para que este fichero quede lo mas limpio y ordenado posible(no se si asi estaria bien hecho)
//importo el css general del fichero Chat aunque cada componente va a tener un poquito de css ordenado por comonentes
import './chat.css'
import Header from './components/Header/Header'
import Message from './components/Message/Message'
import SelectUser from './components/SelectUser/SelectUser'
import SendMessages from './components/SendMessages/SendMessages'
import useUpdateMessages from './hooks/useUpdateMessages'
import { useState } from 'react/cjs/react.development'





const Chat = () => {

    //hooks que hemos tenido que crear aqui porque compartian datos con varios componentes
    const [messages, setMessages] = useUpdateMessages()

    const [selectedUser, setSelectedUser] = useState(null)
    return (
    <>
        <section className='compoMessages'>
            <h1>Chat de anonimos</h1>
            <div className='alingHeader'>
                <Header/>
            </div>
                
            <main className='bodyMessages'>
                {/*Necesita la lista de mensajes del servidor para poder hacer un map y asi pintarlso todos  */}
                <Message messages={messages} setMessages={setMessages}/>
                {/* Necesita  las dos variables lo que vale selectedUser y la funcion para actualizar este valor, para llevar acabo susu funciones*/}
                <SelectUser selectedUser={selectedUser} setSelectedUser={setSelectedUser}/>       
                {/* Necesita valores de lso dos componentes anterirores para actualizar los datos del servidor cada vez que se escribe un mensaje nuevo y ademas para que el mensaje lo escriba el personaje seleccionado  */}
                <SendMessages messages={messages} setMessages={setMessages} selectedUser={selectedUser}/>
            </main>
        </section>
            <footer/>
    </>
    )
}

export default Chat;

//Avatar